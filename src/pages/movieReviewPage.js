import React, {Suspense, lazy} from "react";
import { useLocation } from "react-router-dom";

import MovieReview from "../components/movieReview";
import { useQuery } from "react-query";
import { getMovie } from "../api/tmdb-api";
import Spinner from "../components/spinner";
const PageTemplate = lazy(() => import("../components/templateMoviePage"));

const MovieReviewPage = (props) => {
  let location = useLocation();
  const { movie, review } = location.state;

  const { data: movieData, error, isLoading, isError } = useQuery(
    ["movie", { id: movie.id }],
    () => getMovie(movie.id), 
    {
      staleTime: 30000
    }
  );

  if (isLoading) {
    return <Spinner />;
  }

  if (isError) {
    return <h1>{error.message}</h1>;
  }

  return (
    <Suspense fallback={<Spinner />}>
    <PageTemplate movie={movieData}>
      <MovieReview review={review} />
    </PageTemplate>
    </Suspense>
  );
};

export default MovieReviewPage;
