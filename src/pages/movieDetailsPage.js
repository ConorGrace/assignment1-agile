import React, { Suspense, lazy} from "react";
import { useParams } from 'react-router-dom';
import MovieDetails from "../components/movieDetails/";

import { getMovie, getMovieCredits } from '../api/tmdb-api'
import { useQuery } from "react-query";
import Spinner from '../components/spinner'
import CreditsCard from '../components/creditsCard'; 
const PageTemplate = lazy(() => import("../components/templateMoviePage"));

const MoviePage = (props) => {
  const { id } = useParams();
  const { data: movie, error, isLoading, isError } = useQuery(
    ["movie", { id: id }],
    getMovie
  );

  const { data: creditsData } = useQuery(
    ["movieCredits", { id: id }],
    () => getMovieCredits(id) 
  );

  if (isLoading || !creditsData) {
    return <Spinner />;
  }

  if (isError) {
    return <h1>{error.message}</h1>;
  }

  return (
    <>
      {movie ? (
        <Suspense fallback={<Spinner />}>
        <PageTemplate movie={movie}>
          <MovieDetails movie={movie} />
          <h2>Movie Credits</h2>
          <div>
            {creditsData.map((actor) => (
              <CreditsCard key={actor.id} actor={actor} />
            ))}
          </div>
        </PageTemplate>
        </Suspense>
      ) : (
        <p>Waiting for movie details</p>
      )}
    </>
  );
};

export default MoviePage;
