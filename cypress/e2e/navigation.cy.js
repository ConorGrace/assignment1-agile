let movies;
let movieId = 872585;

describe("Navigation", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        movies = response.results;
      });
  });
  beforeEach(() => {
    cy.visit("/");
  });
  describe("From the home page to a movie's details", () => {
    it("navigates to the movie details page and change browser URL", () => {
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
      cy.url().should("include", `/movies/${movies[0].id}`);
    });
  });
  describe("Using the site header", () => {
    describe("when the viewport is desktop scale to the new and old menu options", () => {
      it("navigate via the button links", () => {
        // the first 2 and last of these are old leftover code from before
        cy.get("button").contains("Favorites").click();
        cy.url().should("include", `/favorites`);
        cy.get("button").contains("Upcoming").click();
        cy.url().should("include", `/upcoming`);
        // These 2 are fresh code that i made as part of web app assignment
        cy.get("button").contains("Top Rated").click();
        cy.url().should("include", `/toprated`);
        cy.get("button").contains("Actors").click();
        cy.url().should("include", `/actors`);
        cy.get("button").contains("Home").click();
        cy.url().should("include", `/`);
      });
    });
    describe(
      "when the viewport is mobile scale",
      {
        viewportHeight: 896,
        viewportWidth: 414,
      },
      () => {
        it("navigate via the dropdown menu to old and new added menu options", () => {
          // the first 2 and last of these are old leftover code from before
          cy.get("header").find("button").click();
          cy.get("li").contains('Favorites').click();
          cy.url().should("include", `/favorites`);
          cy.get("li").contains("Upcoming").click();
          cy.url().should("include", `/upcoming`);
          // These 2 are fresh code that i made as part of web app assignment
          cy.get("li").contains("Top Rated").click();
          cy.url().should("include", `/toprated`);
          cy.get("li").contains("Actors").click();
          cy.url().should("include", `/actors`);
          cy.get("li").contains('Home').click();
          cy.url().should("include", `/`);
        });
      }
    );
  });
  });