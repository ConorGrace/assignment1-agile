let movies; // List of movies from TMDB
let movie; //
let actor = "Al Pacino"

describe("Base tests", () => {
  before(() => {
    // Get the discover movies from TMDB and store them locally.
    cy.request(
      `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body") // Take the body of HTTP response from TMDB
      .then((response) => {
        movies = response.results;
      });
  });
  beforeEach(() => {
    cy.visit("/");
  });

  describe("The Discover Movies page", () => {
    it("displays the page header and 20 movies", () => {
        cy.get("button").contains("Top Rated").click();
      cy.get("h3").contains("Discover Movies");
      cy.get(".MuiCardHeader-root").should("have.length", 20);
    });

  });
  describe("The Movie Details page", () => {
    before(() => {
      // this sets the movie id for the Godfather, which is number 1 rated
      cy.request(
        `https://api.themoviedb.org/3/movie/238?api_key=${Cypress.env("TMDB_KEY")}`
      )
        .its("body")
        .then((movieDetails) => {
          movie = movieDetails;
        });
    });
    beforeEach(() => {
      // this sets the movie id for the Godfather, which is number 1 rated
      cy.visit(`/movies/238`);
    });
    it(" displays the movie title, overview and genres and ", () => {
      cy.get("h3").contains(movie.title);
      cy.get("h3").contains("Overview");
      cy.get("h3").next().contains(movie.overview);
      cy.get("p")
        .next()
        .within(() => {
          const genreChips = movie.genres.map((g) => g.name);
          genreChips.unshift("Genres");
          cy.get("span").each(($card, index) => {
            cy.wrap($card).contains(genreChips[index]);
          });
        });
    });

    it("displays the movie release date in MovieDetails", () => {
     // Checks if the label "Released" exists in a Chip element
    cy.contains("Released").should("exist");

    // Checks if the release date is displayed within the Chip
    cy.get("ul").contains(`Released: ${movie.release_date}`).should("exist");
    });
    it("displays production companies in MovieDetails", () => {
        // This checks if the label "Production Companies" exists in a Chip element
        cy.contains("Production Companies").should("exist");
      
        //This tests if the production company does exist
        movie.production_countries.forEach((company) => {
          cy.get("ul").contains(company.name).should("exist");
        });
      });
  });
  });
