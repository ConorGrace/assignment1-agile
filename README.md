# Assignment 1 - Agile Software Practice.

__Name:__ Conor Grace
__Video:__ https://youtu.be/g9c92jhbH04
__gitlab:__ https://gitlab.com/ConorGrace/assignment1-agile

This repository contains the implementation of a React App, its associated Cypress tests and the GitLab CI pipeline.

## React App Features.

[ Provide a bullet-point list of the __new features__ you added to the React Movies app, as well as any modifications to existing features).] e.g.
 
+ Added a Top Rated Movies Page
+ Added a Top Actors Page
+ Added the Movie Credits to the Movie Details Page

## Automated Tests.

### Unique functionality testing (if relevant).

[Briefly explain the parts of your app's  functionality that are unique and state the associated test file name.] 

e.g.

__Top Movie Page__ - The user can travel to the newly created Top Rated Page

+ cypress/e2e/navigation.cy.js & cypress/e2e/popular.cy.js

__Top Actors Page__ - The user can travel to the newly created Top Actors Page

+ cypress/e2e/navigation.cy.js

__Viewing Movie Credits__ - The user can view Movie Details page to now see the movie credits

+ cypress/e2e/startup.cy.js & cypress/e2e/popular.cy.js

### Error/Exception testing (if relevant).

[State the cases that demonstrate error and/or exception testing.]

1. Displays no movies if wrong searchterm put into filter


### Cypress Custom commands (if relevant).

[ Specify the test file(s) that use a custom Cypress command(s) that you implemented.]

None

## Code Splitting.

[Specify the pathname of each source code file that contains evidence of code splitting in your React app.]

e.g.
+ src/pages/addMovieReview.js
+ src/pages/favoriteMoviesPage.js
+ src/pages/homePage.js
+ src/pages/movieDetailsPage.js
+ src/pages/movieReviewPage.js


## Pull Requests.

I used Gitlab Merge

## Independent learning (If relevant).

[ Briefly explain the work you did to satisfy the requirements of the Outstanding grade category, and include proof (e.g. screenshots) of its success. Also, mention the files that contain evidence of this work.

Didnt go for Outstanding grade

State any other evidence of independent learning achieved while completing this assignment.